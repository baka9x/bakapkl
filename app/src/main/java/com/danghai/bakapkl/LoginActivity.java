package com.danghai.bakapkl;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.danghai.bakapkl.service.APIService;
import com.danghai.bakapkl.service.DataService;
import com.google.android.material.textfield.TextInputEditText;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    EditText email;
    EditText password;
    Button btnLogin, btnClear;
    CheckBox checkSave;
    SharedPreferences data;
    TextView tvForgotPassword, tvRegister;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //anh xa
        init();
        //save form from shareref
        saveInfo();

        //Xử lý click
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                email.setText("");
                password.setText("");
                checkSave.setChecked(false);
            }
        });

//        tvForgotPassword.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(LoginActivity.this, ForgetPasswordActivity.class);
//                intent.putExtra("EMAIL", email.getText().toString().trim());
//                startActivity(intent);
//            }
//        });

        tvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String txtEmail = email.getText().toString().toLowerCase().trim();
                final String txtPassword = password.getText().toString().trim();

                //Validate
                if (TextUtils.isEmpty(txtEmail)) {
                    Toast.makeText(LoginActivity.this, R.string.user_login_validate_email, Toast.LENGTH_SHORT).show();
                }else if(TextUtils.isEmpty(txtPassword)){
                    Toast.makeText(LoginActivity.this, R.string.user_login_validate_password, Toast.LENGTH_SHORT).show();
                }
                else {

                    DataService dataService = APIService.getService();

                    Call<ResponseBody> callback = dataService.loginUser(txtEmail, txtPassword);
                    callback.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            if (response.code() == 200){
                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                startActivity(intent);
                                Log.d("Respsonse", response.toString() + "");
                                Toast.makeText(LoginActivity.this, R.string.user_login_successful, Toast.LENGTH_SHORT).show();

                                SharedPreferences.Editor editor = data.edit();
                                editor.putString("username_loged_in", txtEmail);
                                if (checkSave.isChecked()) {
                                    //Luu tru thong tin vao file
                                    editor.putString("email", txtEmail);
                                    editor.putString("password", txtPassword);
                                    editor.putBoolean("save_information", true);
                                }else{
                                    editor.putString("email", "");
                                    editor.putString("password", "");
                                    editor.putBoolean("save_information", false);
                                }

                                editor.apply();
                                intent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                finish();
                            }else{
                                Toast.makeText(LoginActivity.this, R.string.user_login_failed, Toast.LENGTH_SHORT).show();
                            }

                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });


    }

    public void init(){
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        btnLogin = findViewById(R.id.btnLogin);
        btnClear = findViewById(R.id.btnClear);
        checkSave = findViewById(R.id.checkSave);
        tvForgotPassword = findViewById(R.id.forgetPassword);
        tvRegister = findViewById(R.id.tvRegister);
    }
    public void saveInfo(){
        data = getSharedPreferences("LoginInfoSaved", Context.MODE_PRIVATE);
        //Nạp thông tin lên form từ sharedPreference
        Boolean dataSave = data.getBoolean("save_information", false);
        if (dataSave) {
            email.setText(data.getString("email", ""));
            password.setText(data.getString("password", ""));
            checkSave.setChecked(true);
        }
    }
}
