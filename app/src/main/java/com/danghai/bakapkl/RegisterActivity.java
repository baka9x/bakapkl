package com.danghai.bakapkl;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.danghai.bakapkl.service.APIService;
import com.danghai.bakapkl.service.DataService;

import org.w3c.dom.Text;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {
    EditText fullname, email, password, rePassword;
    Button btnRegister, btnClear;

    TextView tvLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        init();

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fullname.setText("");
                email.setText("");
                password.setText("");
                rePassword.setText("");
            }
        });


        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String txtFullName = fullname.getText().toString().trim();
                String txtEmail = email.getText().toString().toLowerCase().trim();
                String txtPassword = password.getText().toString().trim();
                String txtRePassword = rePassword.getText().toString().trim();

                //Validate
                if (TextUtils.isEmpty(txtFullName)){
                    Toast.makeText(RegisterActivity.this, R.string.user_register_validate_fullname, Toast.LENGTH_SHORT).show();
                }else if (TextUtils.isEmpty(txtEmail)){
                    Toast.makeText(RegisterActivity.this, R.string.user_register_validate_email, Toast.LENGTH_SHORT).show();
                }else if (TextUtils.isEmpty(txtPassword)){
                    Toast.makeText(RegisterActivity.this, R.string.user_register_validate_password, Toast.LENGTH_SHORT).show();
                }else if (TextUtils.isEmpty(txtPassword)){
                    Toast.makeText(RegisterActivity.this, R.string.user_register_validate_rePassword, Toast.LENGTH_SHORT).show();
                }else{

                    registerUser(txtFullName, txtPassword, txtEmail);



                }
            }
        });
    }

    private void registerUser(String txtFullName, String txtPassword, String txtEmail) {

        DataService dataService = APIService.getService();
        Call<ResponseBody> callBack = dataService.createUser(txtFullName, txtPassword, txtEmail);
        callBack.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200){
                    Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                    startActivity(intent);

                    Log.d("Respsonse", response + "");
                    Toast.makeText(RegisterActivity.this, R.string.user_register_successful, Toast.LENGTH_SHORT).show();
                    intent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    finish();
                }else{
                    Toast.makeText(RegisterActivity.this, R.string.user_register_failed, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(RegisterActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void init() {
        fullname = findViewById(R.id.fullname);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        rePassword = findViewById(R.id.rePassword);
        btnRegister = findViewById(R.id.btnRegister);
        btnClear = findViewById(R.id.btnClear);
        tvLogin = findViewById(R.id.tvLogin);

        tvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });


    }
}
