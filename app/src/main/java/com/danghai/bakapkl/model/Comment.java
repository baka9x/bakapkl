package com.danghai.bakapkl.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Comment {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("userId")
    @Expose
    private User userId;
    @SerializedName("cmt_content")
    @Expose
    private String cmtContent;
    @SerializedName("postId")
    @Expose
    private Post postId;
    @SerializedName("timeAt")
    @Expose
    private String timeAt;

    public Comment(User userId, String cmtContent, Post postId, String timeAt) {
        this.userId = userId;
        this.cmtContent = cmtContent;
        this.postId = postId;
        this.timeAt = timeAt;
    }

    public Comment(User userId, String cmtContent, Post postId) {
        this.userId = userId;
        this.cmtContent = cmtContent;
        this.postId = postId;
    }

    public Comment() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public String getCmtContent() {
        return cmtContent;
    }

    public void setCmtContent(String cmtContent) {
        this.cmtContent = cmtContent;
    }

    public Post getPostId() {
        return postId;
    }

    public void setPostId(Post postId) {
        this.postId = postId;
    }

    public String getTimeAt() {
        return timeAt;
    }

    public void setTimeAt(String timeAt) {
        this.timeAt = timeAt;
    }

}