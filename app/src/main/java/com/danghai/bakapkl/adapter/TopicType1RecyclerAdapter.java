package com.danghai.bakapkl.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.danghai.bakapkl.R;
import com.danghai.bakapkl.model.Post;
import com.squareup.picasso.Picasso;

import java.util.List;

public class TopicType1RecyclerAdapter extends RecyclerView.Adapter<TopicType1RecyclerAdapter.ViewHolder>{
    private Context mContext;
    List<Post> mList;

    private OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
        void onOptionItemClick(int position);

    }

    public void setItemClickListener(OnItemClickListener mListener) {
        this.mListener = mListener;
    }

    public TopicType1RecyclerAdapter(Context mContext, List<Post> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }


    @NonNull
    @Override
    public TopicType1RecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_topic1, parent, false);
        return new TopicType1RecyclerAdapter.ViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull TopicType1RecyclerAdapter.ViewHolder holder, int position) {

        Post post = mList.get(position);

        holder.tv_topic_title.setText(post.getTitle());
        holder.tv_topic_created_at.setText(post.getCreatedAt());
        Picasso.get().load(post.getThumbnailUrl()).error(R.drawable.bg).into(holder.iv_topic_img);

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    public class ViewHolder  extends RecyclerView.ViewHolder{

        ImageView iv_topic_img, iv_option_item;
        TextView tv_topic_title,tv_topic_created_at;


        public ViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);

            iv_topic_img = itemView.findViewById(R.id.iv_topic_img);
            tv_topic_title = itemView.findViewById(R.id.tv_topic_title);
            iv_option_item = itemView.findViewById(R.id.iv_option_item);
            tv_topic_created_at = itemView.findViewById(R.id.tv_topic_created_at);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });

            iv_option_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

        }
    }
}
