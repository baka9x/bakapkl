package com.danghai.bakapkl.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.danghai.bakapkl.R;
import com.danghai.bakapkl.model.User;

import java.util.List;

public class UserRecyclerAdapter extends RecyclerView.Adapter<UserRecyclerAdapter.ViewHolder> {

    private Context mContext;
    private List<User> mList;

    public UserRecyclerAdapter(Context mContext, List<User> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @NonNull
    @Override
    public UserRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user, parent, false);
        return new UserRecyclerAdapter.ViewHolder(view);
    }

    
    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull UserRecyclerAdapter.ViewHolder holder, int position) {
            User user = mList.get(position);

            holder.tv_full_name.setText(mContext.getString(R.string.user_info_full_name) + " " + user.getFullname());
            holder.tv_email.setText(mContext.getString(R.string.user_info_email) + " " + user.getEmail());
    }


    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_full_name, tv_email;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_full_name = itemView.findViewById(R.id.tv_full_name);
            tv_email = itemView.findViewById(R.id.tv_email);


        }
    }
}
