package com.danghai.bakapkl.service;

import android.util.Log;

import com.danghai.bakapkl.R;

public class APIService {

    private static final String BASE_URL = "http://192.168.3.187:5003/";


    public static DataService getService(){
        Log.d("URL_BASE", BASE_URL);

        return APIRetrofitClient.getClient(BASE_URL).create(DataService.class);
    }
}
