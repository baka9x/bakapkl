package com.danghai.bakapkl.service;

import com.danghai.bakapkl.model.Category;
import com.danghai.bakapkl.model.Comment;
import com.danghai.bakapkl.model.Post;
import com.danghai.bakapkl.model.User;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;


public interface DataService {

    @FormUrlEncoded
    @POST("api/v1/user/add")
    Call<ResponseBody> createUser(
            @Field("fullname") String fullname,
            @Field("password") String password,
            @Field("email") String email
    );

    @FormUrlEncoded
    @POST("api/v1/login")
    Call<ResponseBody> loginUser(
            @Field("email") String email,
            @Field("password") String password
    );

    @GET("api/v1/users")
    Call<List<User>> getAllUser();


    @GET("api/v1/categories")
    Call<List<Category>> getAllCategories();

    @GET("api/v1/posts")
    Call<List<Post>> getAllPosts();

    //Stats Counter
    @Headers("Content-Type: text/html")
    @GET("api/v1/getUserCounter")
    Call<String> getUserCounter();

    @Headers("Content-Type: text/html")
    @GET("api/v1/getPostCounter")
    Call<String> getPostCounter();

    @Headers("Content-Type: text/html")
    @GET("api/v1/getCategoryCounter")
    Call<String> getCategoryCounter();

    @GET("api/v1/user/email/{email}")
    Call<List<User>> getProfile(@Path("email") String email);

    @GET("api/v1/posts/category/{id}")
    Call<List<Post>> getPostsOrderByCategory(@Path("id") String id);

    //Comment
    @FormUrlEncoded
    @POST("api/v1/comment/add")
    Call<ResponseBody> createComment(
            @Field("userId") String userId,
            @Field("cmt_content") String cmt_content,
            @Field("postId") String postId,
            @Field("timeAt") String timeAt
    );






    

}
