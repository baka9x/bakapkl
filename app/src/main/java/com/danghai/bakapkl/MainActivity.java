package com.danghai.bakapkl;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;

import com.danghai.bakapkl.fragment.HomeFragment;
import com.danghai.bakapkl.fragment.MemberFragment;
import com.danghai.bakapkl.fragment.MoreFragment;
import com.danghai.bakapkl.fragment.StatsFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {
    private SharedPreferences data;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                            new HomeFragment()).commit();
                    break;

                case R.id.navigation_stats:
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                            new StatsFragment()).commit();
                    break;

                case R.id.navigation_social:
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                            new MemberFragment()).commit();
                    break;
                case R.id.navigation_more:
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                            new MoreFragment()).commit();
                    break;

            }
            return true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        data = getSharedPreferences("LoginInfoSaved", Context.MODE_PRIVATE);
        if (data.getString("username_loged_in", "").equals("")){
            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            intent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

        //Default
        if (savedInstanceState == null){

            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    new HomeFragment()).commit();
        }
    }
}
