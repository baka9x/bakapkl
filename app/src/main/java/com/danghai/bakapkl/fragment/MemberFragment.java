package com.danghai.bakapkl.fragment;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.danghai.bakapkl.R;
import com.danghai.bakapkl.adapter.UserRecyclerAdapter;
import com.danghai.bakapkl.model.User;
import com.danghai.bakapkl.service.APIService;
import com.danghai.bakapkl.service.DataService;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MemberFragment extends Fragment {


    RecyclerView rv_user_list;
    UserRecyclerAdapter userRecyclerAdapter;

    List<User> mUser;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_member, container, false);

        //Toolbar
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Danh sách thành viên");



        rv_user_list = view.findViewById(R.id.rv_user_list);
        rv_user_list.setHasFixedSize(true);
        rv_user_list.setLayoutManager(new LinearLayoutManager(getContext()));


        DataService dataService = APIService.getService();
        Call<List<User>> callBack = dataService.getAllUser();

        callBack.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                if (response.isSuccessful()){
                    mUser = new ArrayList<>();
                    mUser = response.body();
                    userRecyclerAdapter = new UserRecyclerAdapter(getContext(), mUser);
                    rv_user_list.setAdapter(userRecyclerAdapter);
                }
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                Toast.makeText(getContext(), t.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        });


        return view;
    }
}
