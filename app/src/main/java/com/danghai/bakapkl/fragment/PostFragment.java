package com.danghai.bakapkl.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.danghai.bakapkl.R;
import com.danghai.bakapkl.model.Comment;

import com.danghai.bakapkl.model.User;
import com.danghai.bakapkl.service.APIService;
import com.danghai.bakapkl.service.DataService;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;


import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PostFragment extends Fragment {
    TextView tv_post_title, tv_post_created_at, tv_post_content, tv_comment_user_logged_in;
    EditText edt_text_comment;
    Button btnComment;
    SharedPreferences data;

    List<User> mUser;
    List<Comment> mComment;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_post, container, false);

        //Toolbar
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Bài đăng");

        tv_post_title = view.findViewById(R.id.tv_post_title);
        tv_post_created_at = view.findViewById(R.id.tv_post_created_at);
        tv_post_content = view.findViewById(R.id.tv_post_content);
        tv_comment_user_logged_in = view.findViewById(R.id.tv_comment_user_logged_in);
        edt_text_comment = view.findViewById(R.id.edt_text_comment);
        btnComment = view.findViewById(R.id.btnComment);

        mUser = new ArrayList<>();

        Bundle bundle = getArguments();
        final String txtPostId = bundle.getString("post_id");
        String txtPostTitle = bundle.getString("post_title");
        String txtPostCreatedAt = bundle.getString("post_created_at");
        String txtPostContent = bundle.getString("post_content");

        //Set Data
        tv_post_title.setText(txtPostTitle);
        tv_post_created_at.setText(txtPostCreatedAt);
        tv_post_content.setText(txtPostContent);


        //Get currentUserName
        data = Objects.requireNonNull(getContext()).getSharedPreferences("LoginInfoSaved", Context.MODE_PRIVATE);
        String txtUsernameLogged = data.getString("username_loged_in", "");

        final DataService dataService = APIService.getService();

        Call<List<User>> callBack = dataService.getProfile(txtUsernameLogged);

        callBack.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                if (response.isSuccessful()) {
                    mUser = response.body();
                    //get currentUser
                    for (User user : mUser) {
                        tv_comment_user_logged_in.setText(user.getFullname());

                        //get curentUserId
                        String currentUserId = user.getId();


                        Log.d("InforComment", currentUserId + "\n" + txtPostId);


                    }
                }

            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                Toast.makeText(getContext(), t.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        });
        final String txtCommentContent = edt_text_comment.getText().toString().trim();
        //txtPostId
        //mComment = new ArrayList<>();

        //mComment.add(new Comment(new User("5e9b45dd52f4006a9b143f75"), "dsadsa", new Post("dasdsadasdsadas")));



        //Add Comment if found currentUserId
        btnComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                Date date = new Date();
                Call<ResponseBody> callBackCmt = dataService.createComment("5e9b45dd52f4006a9b143f75", txtCommentContent, txtPostId, formatter.format(date));
                callBackCmt.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            Log.d("dsadsadsa", response.message() +"");
                        if (response.isSuccessful()) {
                            if (response.code() == 200) {
                                Toast.makeText(getContext(), R.string.user_comment_successful, Toast.LENGTH_SHORT).show();
                                refreshFragment();
                            } else {
                                Toast.makeText(getContext(), R.string.user_comment_failed, Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            Toast.makeText(getContext(), response.errorBody() + "", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });

                Toast.makeText(getContext(), "Clicked!", Toast.LENGTH_SHORT).show();
            }


        });

//        if (response.isSuccessful()){
//            if (response.code() == 200){
//                Toast.makeText(getContext(), R.string.user_comment_successful, Toast.LENGTH_SHORT).show();
//                refreshFragment();
//            }else{
//                Toast.makeText(getContext(), R.string.user_comment_failed, Toast.LENGTH_SHORT).show();
//            }
//        }



        return view;
    }

    //Refresh Fragment when clicked btnResetCart
    private void refreshFragment() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            assert getFragmentManager() != null;
            getFragmentManager().beginTransaction().detach(this).commitNow();
            getFragmentManager().beginTransaction().attach(this).commitNow();

        } else {
            assert getFragmentManager() != null;
            getFragmentManager().beginTransaction().detach(this).attach(this).commit();
        }
    }
}
