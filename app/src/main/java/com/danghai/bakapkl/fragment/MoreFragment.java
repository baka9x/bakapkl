package com.danghai.bakapkl.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.danghai.bakapkl.LoginActivity;
import com.danghai.bakapkl.R;
import com.danghai.bakapkl.model.User;
import com.danghai.bakapkl.service.APIService;
import com.danghai.bakapkl.service.DataService;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MoreFragment extends Fragment {

    TextView more_username, more_user_change_password,more_logout;
    List<User> mUser;
    SharedPreferences data;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_more, container, false);
        //init
        more_username = view.findViewById(R.id.more_username);
        more_user_change_password = view.findViewById(R.id.more_user_change_password);
        more_logout = view.findViewById(R.id.more_logout);
        data = Objects.requireNonNull(getContext()).getSharedPreferences("LoginInfoSaved", Context.MODE_PRIVATE);

        String txtUsernameLogged = data.getString("username_loged_in", "");

        DataService dataService = APIService.getService();

        Call<List<User>> callBack = dataService.getProfile(txtUsernameLogged);

        Log.d("username_loged", txtUsernameLogged);

        callBack.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                mUser = new ArrayList<>();
                mUser = response.body();
                //get currentUser
                for (User user: mUser){
                    more_username.setText(user.getFullname());
                }

            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                Toast.makeText(getContext(), t.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        });

        more_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                  SharedPreferences.Editor editor = data.edit();
                    editor.putString("username_loged_in", "");
                    editor.apply();
                    updateDetail();
            }
        });




        return view;
    }
    public void updateDetail() {
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        intent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
