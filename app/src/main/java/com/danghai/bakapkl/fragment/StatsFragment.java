package com.danghai.bakapkl.fragment;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.danghai.bakapkl.R;
import com.danghai.bakapkl.service.APIService;
import com.danghai.bakapkl.service.DataService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class StatsFragment extends Fragment {

    TextView stats_users, stats_posts, stats_categories;
    String userCounter, postCounter, categoryCounter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_stats, container, false);

        //Toolbar
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Thống kê");

        stats_users = view.findViewById(R.id.stats_users);
        stats_posts = view.findViewById(R.id.stats_posts);
        stats_categories = view.findViewById(R.id.stats_categories);

        userCounter = "";
        postCounter = "";
        categoryCounter = "";


        DataService dataService = APIService.getService();
        Call<String> callBackUserCounter = dataService.getUserCounter();
        Call<String> callBackPostCounter = dataService.getPostCounter();
        Call<String> callBackCategoryCounter = dataService.getCategoryCounter();
        //User counter
        callBackUserCounter.enqueue(new Callback<String>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    userCounter = response.body();
                    stats_users.setText(getString(R.string.stats_user_title) + " " + userCounter);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(getContext(), "Error User Stats: " + t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
        //Post counter
        callBackPostCounter.enqueue(new Callback<String>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    postCounter = response.body();
                    stats_posts.setText(getString(R.string.stats_post_title) + " " + postCounter);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(getContext(), "Error Post Stats: " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        //Category counter
        callBackCategoryCounter.enqueue(new Callback<String>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    categoryCounter = response.body();
                    stats_categories.setText(getString(R.string.stats_category_title) + " " + categoryCounter);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(getContext(), "Error Category Stats: " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }

    //Refresh Fragment when clicked btnResetCart
    private void refreshFragment() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            assert getFragmentManager() != null;
            getFragmentManager().beginTransaction().detach(this).commitNow();
            getFragmentManager().beginTransaction().attach(this).commitNow();

        } else {
            assert getFragmentManager() != null;
            getFragmentManager().beginTransaction().detach(this).attach(this).commit();
        }
    }
}
