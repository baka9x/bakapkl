package com.danghai.bakapkl.fragment;

import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.danghai.bakapkl.R;
import com.danghai.bakapkl.adapter.ViewPagerAdapter;
import com.danghai.bakapkl.model.Category;
import com.danghai.bakapkl.model.Post;
import com.danghai.bakapkl.model.User;
import com.danghai.bakapkl.service.APIService;
import com.danghai.bakapkl.service.DataService;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HomeFragment extends Fragment {
    TabLayout tabLayout;
    ViewPager viewPager;
    ViewPagerAdapter pagerAdapter;

    List<Category> mCategory;
    List<Post> mPost;

    boolean checkLayoutChange = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        mCategory = new ArrayList<>();
        mPost = new ArrayList<>();
        //Toolbar
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Trang chủ");
        //Tab
        tabLayout = view.findViewById(R.id.tabLayout);
        viewPager = view.findViewById(R.id.viewPager);
        pagerAdapter = new ViewPagerAdapter(getChildFragmentManager());


        DataService dataService = APIService.getService();
        Call<List<Category>> callBack = dataService.getAllCategories();
        callBack.enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                if (response.isSuccessful()) {
                    mCategory = response.body();

                    for (int i = 0; i < mCategory.size(); i++) {
                        pagerAdapter.addFragment(new TopicFragment(mCategory.get(i).getId()), mCategory.get(i).getName());
                        pagerAdapter.notifyDataSetChanged();
                        //Log.d("CategoryLog", mCategory.get(i).getId());
                    }

                    viewPager.setAdapter(pagerAdapter);
                    tabLayout.setupWithViewPager(viewPager);

                }

            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {

            }
        });


        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.layout_change, menu);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.layout_change_acts:
                refreshFragment();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    //Refresh Fragment when clicked btnResetCart
    private void refreshFragment() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            assert getFragmentManager() != null;
            getFragmentManager().beginTransaction().detach(this).commitNow();
            getFragmentManager().beginTransaction().attach(this).commitNow();

        } else {
            assert getFragmentManager() != null;
            getFragmentManager().beginTransaction().detach(this).attach(this).commit();
        }
    }
}
