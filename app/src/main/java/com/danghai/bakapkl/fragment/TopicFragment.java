package com.danghai.bakapkl.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.danghai.bakapkl.R;
import com.danghai.bakapkl.adapter.TopicType1RecyclerAdapter;
import com.danghai.bakapkl.model.Category;
import com.danghai.bakapkl.model.Post;
import com.danghai.bakapkl.model.User;
import com.danghai.bakapkl.service.APIService;
import com.danghai.bakapkl.service.DataService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TopicFragment extends Fragment {
    TopicType1RecyclerAdapter topicType1RecyclerAdapter;

    List<Post> mPost;

    RecyclerView rv_topic_list;

    String categoryId;

    public TopicFragment() {
    }

    boolean layoutChange = false;

    public TopicFragment(String categoryId) {
        this.categoryId = categoryId;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_topic, container, false);

        init(view);

        return view;
    }

    private void init(View view) {

        rv_topic_list = view.findViewById(R.id.rv_topic_list);
        mPost = new ArrayList<>();

        DataService dataService = APIService.getService();
        Call<List<Post>> callBack = dataService.getPostsOrderByCategory(categoryId);

        callBack.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                if (response.isSuccessful()){
                    mPost = response.body();

                        topicType1RecyclerAdapter = new TopicType1RecyclerAdapter(getContext(), mPost);
                        //Gridlayout with 2 cols
                        rv_topic_list.setLayoutManager(new GridLayoutManager(getContext(), 2));
                        rv_topic_list.setAdapter(topicType1RecyclerAdapter);
                        topicType1RecyclerAdapter.setItemClickListener(new TopicType1RecyclerAdapter.OnItemClickListener() {
                            @Override
                            public void onItemClick(int position) {
                                doActionItemClicker(position);
                            }

                            @Override
                            public void onOptionItemClick(int position) {

                            }
                        });

                }
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
                Toast.makeText(getContext(), t.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        });



    }

    private void doActionItemClicker(int position) {
        Post post = mPost.get(position);

        Bundle bundle = new Bundle();

        bundle.putString("post_id", post.getId());
        bundle.putString("post_title", post.getTitle());
        bundle.putString("post_created_at", post.getCreatedAt());
        bundle.putString("post_content", post.getContent());
        //bundle.putString("post_category", post.getCategory());
        //bundle.putString("post_author", post.getAuthor());
        Fragment postFragment = new PostFragment();
        postFragment.setArguments(bundle);
        replaceFragment(postFragment);


    }

    public void replaceFragment(Fragment someFragment) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, someFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
